import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category';
import { CategoriesService } from 'src/app/services/categories.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories: Category[];

  constructor(private _service: CategoriesService, private _route: ActivatedRoute) {
    this.categories = [];
  }

  ngOnInit() {
    this._service.list(this._route).subscribe(categories => this.categories = categories);
  }

}
