import { Component, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductsService } from 'src/app/services/products.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: Product[];

  constructor(private _service: ProductsService, private _route: ActivatedRoute) {
    this.products = [];
  }

  ngOnInit() {
    this._service.list(this._route).subscribe(products => this.products = products);
  }

}
