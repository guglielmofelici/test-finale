import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Category } from '../models/category';
import { environment } from 'src/environments/environment';
import { ICategory } from '../interfaces/category.interface';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private _http: HttpClient) { }

  public list(route: ActivatedRoute): Observable<Category[]> {
    let url: string;
    route.queryParams.subscribe(param => url = `${environment.url}/categories` +
      (param['id'] === undefined ?
        "" : `/?id=${param['id']}`));
    return this._http.get<ICategory[]>(url).pipe(
      map(arr => arr.map(category => new Category(
        category.id, category.name))));
  }
}
