import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/product';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IProduct } from '../interfaces/product.interface';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private _http: HttpClient) {
  }

  public list(route: ActivatedRoute): Observable<Product[]> {
    let url: string;
    route.queryParams.subscribe(param => url = `${environment.url}/products` +
      (param['category_id'] === undefined ?
        "" : `/?category_id=${param['category_id']}`));
    return this._http.get<IProduct[]>(url).pipe(
      map(arr => arr.map(product => new Product(
        product.id, product.name, product.description, product.image, product.price, product.category_id))));
  }
}
