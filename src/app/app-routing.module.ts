import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './components/products/products.component';

const routes: Routes = [{
  component: ProductsComponent,
  path: 'products'
}, {
  path: '',
  pathMatch: 'full',
  redirectTo: 'products'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
